import React from 'react'
import { useParams } from 'react-router';
import FetchData from '../../../services/FetchData';
import "./Content.css";
import SubContent from './SubContent/SubContent';

const Content = () => {

    // const [module, setModule] = useState(null);
    const { module, filter } = useParams();

    const { data, isPending, error } = FetchData(`http://3.108.46.195:9000/${module.toLowerCase()}/view/`);

    return (
        <div className="mainContent">
            <div className="search">
                <div className="searchBar">
                    Search Box {filter}
                </div>
            </div>
            {error && <div>{error}</div>}
            {isPending && <div>Loading...</div>}

            {data &&
                data.results.map(dataItem =>
                (
                    <SubContent key={dataItem.id} data={dataItem} />
                ))
            }
        </div>
    )
}

export default Content;