/* eslint-disable no-unused-vars */
/* eslint-disable no-cond-assign */
/* eslint-disable no-const-assign */
import { useHistory, useParams } from 'react-router';
import './SubContent.css';

const SubContent = (dataItem) => {

    const { module, filter } = useParams();
    const history = useHistory();

    function loadSubContent(moduleContentId) {
        history.push(`${module}/${moduleContentId}`);
    }

    // console.log(filter);

    // console.log(dataItem.data.row_status.status_code);

    return (
        <div>
            {filter === 'approved' &&
                dataItem.data.row_status.status_code === "APPROVED" &&

                <div className="subContent" onClick={() => { loadSubContent(dataItem.data.id) }}>
                    <div className="subContentPic">
                        <img src={dataItem.data.image_link} alt="pic" />
                    </div>
                    <div className="subContentHeading">
                        {dataItem.data.headline}
                    </div>
                    <div className="approval">
                        {dataItem.data.row_status.status_code}
                    </div>
                </div>
            }
            {filter === 'disapproved' &&
                dataItem.data.row_status.status_code === "DISAPPROVED" &&

                <div className="subContent" onClick={() => { loadSubContent(dataItem.data.id) }}>
                    <div className="subContentPic">
                        <img src={dataItem.data.image_link} alt="pic" />
                    </div>
                    <div className="subContentHeading">
                        {dataItem.data.headline}
                    </div>
                    <div className="approval">
                        {dataItem.data.row_status.status_code}
                    </div>
                </div>
            }
            {filter !== 'approved' &&
                filter !== 'disapproved' &&

                <div className="subContent" onClick={() => { loadSubContent(dataItem.data.id) }}>
                    <div className="subContentPic">
                        <img src={dataItem.data.image_link} alt="pic" />
                    </div>
                    <div className="subContentHeading">
                        {dataItem.data.headline}
                    </div>
                    <div className="approval">
                        {dataItem.data.row_status.status_code}
                    </div>
                </div>
            }
        </div>
    )

    // if (filter === 'approved') {
    //     if (dataItem.data.row_status.status_code === "APPROVED") {
    //         console.log(dataItem.data.row_status.status_code);
    //         return (
    //             <div className="subContent" onClick={() => { loadSubContent(dataItem.data.id) }}>
    //                 <div className="subContentPic">
    //                     <img src={dataItem.data.image_link} alt="pic" />
    //                 </div>
    //                 <div className="subContentHeading">
    //                     {dataItem.data.headline}
    //                 </div>
    //                 <div className="approval">
    //                     {dataItem.data.row_status.status_code}
    //                 </div>
    //             </div>
    //         )
    //     }
    // }

    // else if (filter === 'disapproved') {
    //     if (dataItem.data.row_status.status_code === "DISAPPROVED") {
    //         console.log(dataItem.data.row_status.status_code);
    //         return (
    //             <div className="subContent" onClick={() => { loadSubContent(dataItem.data.id) }}>
    //                 <div className="subContentPic">
    //                     <img src={dataItem.data.image_link} alt="pic" />
    //                 </div>
    //                 <div className="subContentHeading">
    //                     {dataItem.data.headline}
    //                 </div>
    //                 <div className="approval">
    //                     {dataItem.data.row_status.status_code}
    //                 </div>
    //             </div>
    //         )
    //     }
    // }

    // else {
    //     return (
    //         <div>else</div>
    //         // <div className="subContent" onClick={() => { loadSubContent(dataItem.data.id) }}>
    //         //     <div className="subContentPic">
    //         //         <img src={dataItem.data.image_link} alt="pic" />
    //         //     </div>
    //         //     <div className="subContentHeading">
    //         //         {dataItem.data.headline}
    //         //     </div>
    //         //     <div className="approval">
    //         //         {dataItem.data.row_status.status_code}
    //         //     </div>
    //         // </div>
    //     )
    // }
}

export default SubContent;