import { useParams } from 'react-router';
import FetchData from '../../../../services/FetchData';
import './SubContentPage.css';

const SubCotentPage = () => {

    const { module, id } = useParams();

    const { data, isPending, error } = FetchData(`http://3.108.46.195:9000/${module.toLowerCase()}/view/?id=${id}`);

    // console.log();

    return (
        <div className="subContentMain">
            <div className="subContentBox">
                {isPending && <div>Loading...</div>}
                {error && <div>{error}</div>}
                {data &&
                    <div>
                        id:
                        <br />
                        {data.results[0].id}
                        <br />
                        <br />
                        Short Description:
                        <br />
                        {data.results[0].short_description}
                        <br />
                        <br />
                        Long Description:
                        <br />
                        {data.results[0].long_description}
                    </div>}
            </div>
        </div>
    )
}

export default SubCotentPage;