import { useState } from 'react';
import { useParams } from 'react-router';
import './Create.css'

const Create = () => {

    const { module } = useParams();

    const [isPending, setisPending] = useState(false);

    const [headline, setHeadline] = useState('');
    const [date, setDate] = useState('');
    const [img_link, setImgLink] = useState('');
    const [short_desc, setShortDesc] = useState('');
    const [long_desc, setLongDesc] = useState('');
    const [tags, setTags] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        const SubmittedData = { headline, date, img_link, short_desc, long_desc, tags };

        setisPending(true);

        console.log(SubmittedData);

        // fetch('http://3.108.46.195:9000/news/create/', {
        //     method: 'POST',
        //     headers: { "Content-Type": "application/json" },
        //     body: JSON.stringify(SubmittedData)
        // }).then(() => {
        //     console.log("data added");
        //     setisPending(false);
        // })
    }

    return (
        <div className="createMain">
            <div className="createBox">
                <div className="createHeading">
                    Create {module}
                </div>
                <div className="createForm">
                    <form onSubmit={handleSubmit}>
                        <div className="form-group">
                            <label>Headline</label>
                            <input
                                type="text"
                                name="headline"
                                value={headline}
                                className="form-control"
                                onChange={(e) => setHeadline(e.target.value)} />
                        </div>
                        <div className="form-group">  <label >Date</label>
                            <input
                                type="date"
                                value={date}
                                className="form-control"
                                name="date"
                                onChange={(e) => setDate(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label >Image Link</label>
                            <input
                                type="text"
                                name="img_link"
                                className="form-control"
                                value={img_link}
                                onChange={(e) => setImgLink(e.target.value)} />
                        </div>
                        <div className="form-group">
                            <label>Short Description</label>
                            <textarea
                                className="form-control"
                                name="short_desc"
                                value={short_desc}
                                onChange={(e) => setShortDesc(e.target.value)} /></div>
                        <div className="form-group">
                            <label> Long Description</label>
                            <textarea
                                className="form-control"
                                name="long-desc"
                                value={long_desc}
                                onChange={(e) => setLongDesc(e.target.value)} /></div>
                        <div className="form-group">
                            <label >Tags</label>
                            <input
                                className="form-control"
                                type="text"
                                value={tags}
                                onChange={(e) => setTags(e.target.value)} />
                        </div>
                    </form>
                    <div className="createBtn">
                        {!isPending && <button className="btn btn-dark">
                            Create
                        </button>}
                        {isPending && <button className="btn btn-dark" disabled>
                            Creating...
                        </button>}
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Create;
