import React from 'react'
import { useHistory, useParams } from 'react-router'
import { Link } from 'react-router-dom';
import './InnerDrawer.css'

const InnerDrawer = (props) => {

    const { module } = useParams();
    const history = useHistory();

    function addSubModule() {
        history.push(`${module}/create`);
    }

    return (
        <div className="innerDrawer">
            <div className="heading">
                <div className="moduleName">
                    {module}
                </div>
            </div>
            <div className="btnDiv ">
                <button onClick={addSubModule}>Add {module}</button>
            </div>
            <div className="filters">
                <ul>
                    <li>
                        <Link to={`/${module}`} >All</Link>
                    </li>
                    <li>
                        <Link to={`/${module}/approved`} >Approved</Link>
                    </li>
                    <li>
                        <Link to={`/${module}/disapproved`}>Disapproved</Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default InnerDrawer;
