import React from 'react'
import { useParams } from 'react-router'
// import FetchData from '../../services/FetchData'
import './Body.css'
import Content from './Content/Content'
import InnerDrawer from './InnerDrawer/InnerDrawer'

const Body = () => {

    const { module } = useParams();

    // useEffect(() => {
    //     if ({ module } !== null) {
    //         const { data, isPending, error } = FetchData(`http://3.108.46.195:9000/${module}/view/`);
    //     }
    //     // console.log({ module });
    // }, [{ module }]);

    return (
        <div className="body">
            <div className="mainBdy">
                <InnerDrawer module={module} />
                <Content />
            </div>
        </div>
    )
}

export default Body;

