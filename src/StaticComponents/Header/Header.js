import React from 'react';
import './Header.css';

const Header = () => {
    return (
        <header className="header">
            <nav className="header_nav">
                <div className="drawerBtn">
                    <i className="fas fa-bars"></i>
                </div>
                <div className="spacer" />
                <div className="header_items">
                    <ul>
                        <li>
                            <a href="/"><i className="far fa-bell"></i></a>
                        </li>
                        <li>
                            <a href="/"><i className="far fa-comment-alt"></i></a>
                        </li>
                        <li>
                            <a href="/"><i className="fas fa-circle"></i></a>
                        </li>
                        <li>
                            <a href="/">
                            Demo Name   <i className="fas fa-chevron-circle-down"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}

export default Header;

