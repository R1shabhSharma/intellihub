import React from 'react'
import { Link } from 'react-router-dom';
import FetchData from '../../services/FetchData';
import './Drawer.css';

const Drawer = () => {

    const { data, isPending, error } = FetchData('http://3.108.46.195:9000/module/view/');

    return (
        <nav className="sideDrawer">
            <div className="Name">
                intellihub
            </div>
            {error && <div>{error}</div>}
            {isPending && <div>Loading...</div>}
            {data && data.map((module) => (
                <div key={module.id} className="moduleOption">
                    <Link to={`/${module.module_name}`}> {module.module_name}</Link>
                </div>
            ))}

        </nav>
    )
}

export default Drawer;