import './App.css';
import Body from './StaticComponents/Body/Body';
// import Backdrop from './StaticComponents/SideDrawer/Backdrop/Backdrop';
import Drawer from './StaticComponents/SideDrawer/Drawer';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './StaticComponents/Header/Header';
import Create from './StaticComponents/Body/Pages/Create/Create';
import SubCotentPage from './StaticComponents/Body/Pages/SubContentPage/SubCotentPage';

function App() {

  return (
    <Router>
      <div className="App">
        {/* <Backdrop /> */}
        <Drawer />
        <div className="headerNbody">
          <Header />
          <Switch>
            <Route exact path="/:module">
              <Body />
            </Route>
            <Route exact path="/:module/:filter">
              <Body />
            </Route>
            <Route exact path="/:module/create">
              <Create />
            </Route>
            <Route exact path="/:module/:id">
              <SubCotentPage />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
